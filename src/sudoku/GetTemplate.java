package sudoku;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;

/**
 *
 * @author philipjnr.habib
 */
public class GetTemplate
{
    private static JButton[][][][] template = new JButton[3][3][3][3];

    public static JButton[][][][] getTemplate(String level)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    for (int l = 0; l < 3; l++)
                    {
                        template[i][j][k][l] = new JButton();
                        template[i][j][k][l].setBackground(Color.white);
                        template[i][j][k][l].setPreferredSize(new Dimension(50, 50));
                    }
                }
            }
        }
        switch (level)
        {
            case "easy":
                return getEasyTemplate();
            case "medium":
                return getMediumTemplate();
            case "hard":
                return getHardTemplate();
            default:
                return null;
        }
    }

    private static JButton[][][][] getEasyTemplate()
    {
        template[0][0][0][2].setText("3");
        template[0][0][1][0].setText("4");
        template[0][0][2][0].setText("2");
        template[0][0][2][1].setText("7");
        template[0][1][0][1].setText("9");
        template[0][1][0][2].setText("2");
        template[0][1][1][1].setText("3");
        template[0][2][1][1].setText("1");
        
        template[1][0][0][1].setText("1");
        template[1][0][1][1].setText("5");
        template[1][0][2][0].setText("3");
        template[1][1][0][0].setText("3");
        template[1][1][1][0].setText("1");
        template[1][1][1][1].setText("6");
        template[1][1][1][2].setText("7");
        template[1][1][2][2].setText("8");
        template[1][2][0][2].setText("8");
        template[1][2][1][1].setText("3");
        template[1][2][2][1].setText("6");
        
        template[2][0][1][1].setText("3");
        template[2][1][1][1].setText("8");
        template[2][1][2][0].setText("6");
        template[2][1][2][1].setText("2");
        template[2][2][0][1].setText("5");
        template[2][2][0][2].setText("3");
        template[2][2][1][2].setText("9");
        template[2][2][2][0].setText("1");
        
        return template;

    }

    private static JButton[][][][] getMediumTemplate()
    {
        template[0][0][0][1].setText("5");
        template[0][0][0][2].setText("6");
        template[0][0][1][2].setText("9");
        template[0][1][1][0].setText("6");
        template[0][1][2][1].setText("2");
        template[0][1][2][2].setText("8");
        
        template[1][0][0][0].setText("8");
        template[1][0][1][0].setText("1");
        template[1][0][1][1].setText("2");
        template[1][0][2][2].setText("7");
        template[1][1][0][2].setText("5");
        template[1][1][2][0].setText("4");
        template[1][2][0][0].setText("9");
        template[1][2][1][1].setText("5");
        template[1][2][1][2].setText("3");
        template[1][2][2][2].setText("2");
        
        template[2][1][0][0].setText("3");
        template[2][1][0][1].setText("1");
        template[2][1][1][2].setText("6");
        template[2][2][1][0].setText("4");
        template[2][2][2][0].setText("8");
        template[2][2][2][1].setText("2");
        
        return template;
        
    }

    private static JButton[][][][] getHardTemplate()
    {
        template[0][0][0][0].setText("1");
        template[0][0][1][1].setText("2");
        template[0][0][2][2].setText("3");
        template[0][1][0][2].setText("4");
        template[0][1][1][2].setText("8");
        template[0][1][2][2].setText("2");
        template[0][2][0][2].setText("8");
        template[0][2][1][1].setText("6");
        template[0][2][2][0].setText("1");
        
        template[1][0][0][0].setText("6");
        template[1][0][0][1].setText("1");
        template[1][0][0][2].setText("2");
        template[1][2][2][0].setText("9");
        template[1][2][2][1].setText("7");
        template[1][2][2][2].setText("4");
        
        template[2][0][2][0].setText("9");
        template[2][0][1][1].setText("4");
        template[2][0][0][2].setText("3");
        template[2][1][0][0].setText("3");
        template[2][1][1][0].setText("7");
        template[2][1][2][0].setText("6");
        template[2][2][0][0].setText("7");
        template[2][2][1][1].setText("8");
        template[2][2][2][2].setText("9");
        
        return template;
    }

}
