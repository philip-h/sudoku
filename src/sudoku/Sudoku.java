package sudoku;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 *
 * @author philipjnr.habib
 */
public class Sudoku extends JFrame
{
    private static final long serialVersionUID = 1L;
    private final JButton[][][][] squares = new JButton[3][3][3][3];
    private JButton[] numbers = new JButton[10];
    private JButton numberButtonPressed;
    private ImageIcon pencil, greenPencil;
    private JFrame optionsFrame;
    private boolean showHints = false;
    

    public Sudoku()
    {
        super("Philip's awesome Sudoku game");
        UIManager.put("Button.disabledText", Color.black);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        setFocusable(true);
        setOptionsFrame();
    }

    private void init()
    {
        JPanel mainPane = new JPanel(new BorderLayout());
        JPanel mainGrid = new JPanel(new GridLayout(3, 3, 10, 10));
        mainGrid.setBackground(Color.black);

        JPanel squareGrids[][] = new JPanel[3][3];
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                squareGrids[i][j] = new JPanel(new GridLayout(3, 3, 1, 1));
                squareGrids[i][j].setBackground(Color.black);
                for (int k = 0; k < 3; k++)
                {
                    for (int l = 0; l < 3; l++)
                    {
                        squares[i][j][k][l] = new JButton();
                        squares[i][j][k][l].setBackground(Color.white);
                        squares[i][j][k][l].setForeground(new Color(128,128,128));
                        squares[i][j][k][l].setPreferredSize(new Dimension(50, 50));
                        squares[i][j][k][l].setFont(new Font("Dialog",Font.PLAIN,15));
                        squares[i][j][k][l].addActionListener(squareButtonListener);
                        squareGrids[i][j].add(squares[i][j][k][l]);

                    }
                }
                mainGrid.add(squareGrids[i][j], BorderLayout.CENTER);
            }
        }

        mainPane.add(mainGrid);

        JPanel buttonPane = new JPanel();
        buttonPane.setBorder(BorderFactory.createEtchedBorder());
        buttonPane.setBackground(Color.lightGray);
        pencil = new ImageIcon(Sudoku.class.getResource("/images/Eraser-3-icon.png"));
        greenPencil = new ImageIcon(Sudoku.class.getResource("/images/Eraser-3-icon-green.png"));
        for (int i = 0; i < 9; i++)
        {
            numbers[i] = new JButton((i + 1) + "");
            numbers[i].setBackground(Color.white);
            numbers[i].setPreferredSize(new Dimension(45, 45));
            numbers[i].addActionListener(numberButtonListener);
            buttonPane.add(numbers[i]);
        }
        numbers[9] = new JButton("", pencil);
        numbers[9].setPreferredSize(new Dimension(45, 45));
        numbers[9].setBackground(Color.white);
        numbers[9].addActionListener(numberButtonListener);
        buttonPane.add(numbers[9]);

        mainPane.add(buttonPane, BorderLayout.SOUTH);
        setContentPane(mainPane);
        
        JMenuBar menuBar = new JMenuBar();
        JMenuItem toggleMenu = new JMenuItem("Toggle Menu");
        toggleMenu.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                optionsFrame.setVisible(!optionsFrame.isVisible());
            }
        });
        menuBar.add(toggleMenu);
        setJMenuBar(menuBar);

    }

    private void setOptionsFrame()
    {
        optionsFrame = new JFrame("Options");
        optionsFrame.setSize(300, 150);
        optionsFrame.setLocation(1250, 230);
        optionsFrame.setVisible(true);

        JPanel mainPanel = new JPanel(new GridLayout(3, 1));
        JPanel checkPanel = new JPanel();
        checkPanel.setBorder(BorderFactory.createEtchedBorder());
        final JLabel checkLabel = new JLabel("Hints are off");
        JCheckBox checkBox = new JCheckBox();
        checkPanel.add(checkLabel);
        checkPanel.add(checkBox);
        mainPanel.add(checkPanel);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(BorderFactory.createEtchedBorder());
        final JButton save = new JButton("Save");
        buttonPanel.add(save);

        final JButton load = new JButton("Load");
        buttonPanel.add(load);

        mainPanel.add(buttonPanel);

        JPanel levelsPane = new JPanel();
        levelsPane.setBorder(BorderFactory.createEtchedBorder());
        final JButton easy = new JButton("Easy");
        levelsPane.add(easy);
        final JButton medium = new JButton("Medium");
        levelsPane.add(medium);
        final JButton hard = new JButton("Hard");
        levelsPane.add(hard);

        mainPanel.add(levelsPane);

        optionsFrame.setContentPane(mainPanel);
        Action hintsAL = new AbstractAction()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                JCheckBox check = (JCheckBox) e.getSource();
                if (check.isSelected())
                {
                    showHints = true;
                    checkLabel.setText("Hints are on");
                } else
                {
                    showHints = false;
                    checkLabel.setText("Hints are off");
                }
            }
        };

        Action saveLoadAL = new AbstractAction()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                JButton button = (JButton) e.getSource();
                if (button == save)
                {
                    try (PrintWriter output = new PrintWriter(new FileWriter("sudoku.save")))
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                for (int k = 0; k < 3; k++)
                                {
                                    for (int l = 0; l < 3; l++)
                                    {
                                        String text = squares[i][j][k][l].getText();
                                        if (text.equals(""))
                                        {
                                            output.print("x ");
                                        } else
                                        {
                                            if (squares[i][j][k][l].isEnabled() == false)
                                            {
                                                output.print(text +"d ");
                                            } else
                                            {
                                                output.print(text + " ");
                                            }
                                        }
                                    }
                                }
                                output.println();
                            }
                        }
                    } catch (IOException ex)
                    {
                        JOptionPane.showMessageDialog(null, "There was an issue writing the save file\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    } finally
                    {
                        JOptionPane.showMessageDialog(null, "Game was successfully saved!", "Success", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else if (button == load)
                {
                    try (Scanner input = new Scanner(new FileReader("sudoku.save")))
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                for (int k = 0; k < 3; k++)
                                {
                                    for (int l = 0; l < 3; l++)
                                    {
                                        if (input.hasNext())
                                        {
                                            String text = input.next();
                                            if (text.equals("x"))
                                            {
                                                squares[i][j][k][l].setText("");
                                                squares[i][j][k][l].setEnabled(true);
                                            } else if (text.contains("d"))
                                            {
                                                squares[i][j][k][l].setText(text.charAt(0)+"");
                                                squares[i][j][k][l].setEnabled(false);
                                            } else
                                            {
                                                squares[i][j][k][l].setText(text);
                                                squares[i][j][k][l].setEnabled(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (FileNotFoundException ex)
                    {
                        JOptionPane.showMessageDialog(null, "There was an issue finding the save file\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        };

        Action levelsAL = new AbstractAction()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                JButton button = (JButton) e.getSource();
                if (button == easy)
                {
                    setSquares(GetTemplate.getTemplate("easy"));
                } else if (button == medium)
                {
                    setSquares(GetTemplate.getTemplate("medium"));
                } else if (button == hard)
                {
                    setSquares(GetTemplate.getTemplate("hard"));
                }
            }
        };
        
        checkBox.addActionListener(hintsAL);
        save.addActionListener(saveLoadAL);
        load.addActionListener(saveLoadAL);
        easy.addActionListener(levelsAL);
        medium.addActionListener(levelsAL);
        hard.addActionListener(levelsAL);

    }

    private void setSquares(JButton[][][][] template)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    for (int l = 0; l < 3; l++)
                    {
                        String text = template[i][j][k][l].getText();
                        if (!text.equals(""))
                        {
                            squares[i][j][k][l].setText(text);
                            squares[i][j][k][l].setEnabled(false);   
                        } else
                        {
                            squares[i][j][k][l].setText("");
                        }

                    }
                }
            }
        }
    }

    private final Action squareButtonListener = new AbstractAction()
    {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e)
        {
            JButton buttonPressed = (JButton) e.getSource();
            if (numberButtonPressed != null && numberButtonPressed.getBackground() == Color.green)
            {
                buttonPressed.setText(numberButtonPressed.getText());
                buttonPressed.setBackground(Color.white);
                if (buttonPressed.getIcon() != null)
                {
                    buttonPressed.setIcon(pencil);
                }
                numberButtonPressed.setBackground(Color.white);
                if (numberButtonPressed.getIcon() == greenPencil)
                {
                    numberButtonPressed.setIcon(pencil);
                }
                if (showHints)
                {
                    checkLegalMove(buttonPressed);
                }
                checkWin();
            }

        }

    };

    private final Action numberButtonListener = new AbstractAction()
    {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e)
        {
            numberButtonPressed = (JButton) e.getSource();
            for (int i = 0; i < 9; i++)
            {
                if (numbers[i].getBackground() == Color.green)
                {
                    numbers[i].setBackground(Color.white);
                }
                if (numbers[9].getIcon() == greenPencil)
                {
                    numbers[9].setIcon(pencil);
                }
            }
            numberButtonPressed.setBackground(Color.green);
            if (numberButtonPressed.getIcon() != null)
            {
                numberButtonPressed.setIcon(greenPencil);
            }
        }

    };

    private int[] getButtonCoords(JButton button)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    for (int l = 0; l < 3; l++)
                    {

                        if (button == squares[i][j][k][l])
                        {
                            return new int[]
                            {
                                i, j, k, l
                            };
                        }
                    }
                }
            }
        }
        return null;
    }

    private void checkLegalMove(JButton button)
    {
        int[] buttonCoords = getButtonCoords(button);
        int boxRow = buttonCoords[0];     //i
        int boxCol = buttonCoords[1];     //j
        int squareRow = buttonCoords[2];  //k
        int squareCol = buttonCoords[3];  //l

        //Check if the big square contains the same number
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                if (k != squareRow || l != squareCol)
                {
                    if (squares[boxRow][boxCol][k][l].getText().equals(button.getText()) && !squares[boxRow][boxCol][k][l].getText().equals(""))
                    {
                        button.setBackground(Color.red);
                    }
                }
            }
        }

        //Check if the row contains the same number
        for (int j = 0; j < 3; j++)
        {
            for (int l = 0; l < 3; l++)
            {
                if (j != boxCol || l != squareCol)
                {
                    if (squares[boxRow][j][squareRow][l].getText().equals(button.getText()) && !squares[boxRow][j][squareRow][l].getText().equals(""))
                    {
                        button.setBackground(Color.red);
                    }
                }
            }
        }

        //Check if the column contains the same number
        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i != boxRow || k != squareRow)
                {
                    if (squares[i][boxCol][k][squareCol].getText().equals(button.getText()) && !squares[i][boxCol][k][squareCol].getText().equals(""))
                    {
                        button.setBackground(Color.red);
                    }
                }
            }
        }
    }

    private void checkWin()
    {
        ArrayList<String> numbersInSquares = new ArrayList<>();
        //Check if all boxes have 9 numbers in them
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    for (int l = 0; l < 3; l++)
                    {
                        numbersInSquares.add(squares[i][j][k][l].getText());
                    }
                }
                if (!isAllNine(numbersInSquares))
                {
                    return;
                }
            }
        }

        //Check if all rows have 9 numbers in them
        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                for (int j = 0; j < 3; j++)
                {
                    for (int l = 0; l < 3; l++)
                    {
                        numbersInSquares.add(squares[i][j][k][l].getText());
                    }
                }
                if (!isAllNine(numbersInSquares))
                {
                    return;
                }
            }
        }

        //Check if all columns have 9 numbers in them
        for (int k = 0; k < 3; k++)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int l = 0; l < 3; l++)
                {

                    for (int j = 0; j < 3; j++)
                    {

                        numbersInSquares.add(squares[i][j][k][l].getText());
                    }
                }
                if (!isAllNine(numbersInSquares))
                {
                    return;
                }
            }
        }

        throwWinningParty();
    }

    private boolean isAllNine(ArrayList<String> numbers)
    {
        for (int i = 1; i < 9; i++)
        {
            if (!numbers.contains(i + ""))
            {
                return false;
            }
        }
        numbers.clear();
        return true;
    }

    private void throwWinningParty()
    {
        int choice = JOptionPane.showConfirmDialog(null, "Wow! Sudoku Wizard! You Won!!!\nClick Cancel to Exit", "Winner Winner", JOptionPane.OK_CANCEL_OPTION);
        if (choice == JOptionPane.CANCEL_OPTION)
        {
            dispose();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        new Sudoku();
    }
}
